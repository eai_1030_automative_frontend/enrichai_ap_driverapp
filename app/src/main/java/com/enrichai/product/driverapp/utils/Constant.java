package com.enrichai.product.driverapp.utils;

/**
 * Created by Priyank on 14-06-2018.
 */

public class Constant {
    public static final String INTERNET_CONNECTION="Please check your internet connection";
    public static final String FORGET_PASSWORD = "Please Contact with Admin!";
    public static final String WELCOME_SCREEN = "WelcomeScreen";
    public static final String SIGN_UP = "SignUp";
    public static final String LOGIN_ACTIVITY = "LoginActivity";
    public static final String MAIN_ACTIVITY = "MainActivity";
    public static final String PROFILE_ACTIVITY = "ProfileActivity";
    public static final String TRIP_ACTIVITY = "TripActivity";
    public static final String TAKE_ACTION = "TakeAction";
}
