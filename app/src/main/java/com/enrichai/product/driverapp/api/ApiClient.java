package com.enrichai.product.driverapp.api;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String TAG = ApiClient.class.getSimpleName();

    private static final boolean production = false;//BuildConfig.DEBUG;


    public static final String BASE_URL = production ? "http://ipl-api-access-layer.eu-gb.mybluemix.net/api/v1/" : "http://ipl-api-access-layer.eu-gb.mybluemix.net/api/v1/";


    public static boolean isProduction() {
        return production;
    }

    private static Retrofit retrofit = null;

    private static ApiClient apiClient;

    private Context context;

    private static final Object mLock = new Object();

    public ApiClient() {
    }

    public ApiClient(Context context) {
        this.context = context;
    }

    public static ApiClient getSingletonApiClient() {
        synchronized (mLock) {
            if (apiClient == null)
                apiClient = new ApiClient();

            return apiClient;
        }
    }

    private static Retrofit getClient() {
        if (retrofit == null) {
            //OkHttpClient.Builder client = new OkHttpClient.Builder();

            OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(60 * 5, TimeUnit.SECONDS)
                    .readTimeout(60 * 5, TimeUnit.SECONDS)
                    .writeTimeout(60 * 5, TimeUnit.SECONDS);

            GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient.build())
                    .addConverterFactory(gsonConverterFactory)
                    .build();

        }
        return retrofit;
    }
/*
    public void login(LoginRequest request, Callback<LoginResponse> callback) {
        Call<LoginResponse> call = null;
        try {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Log.d("Login Request API Call", "Login Request calling");
            Log.d("Login Request API Call", "URL :: " + BASE_URL);
            call = apiService.loginDriver(request);
            Log.d("Login Request", "Peeru Request" + request.toString());
            call.enqueue(callback);
        } catch (Throwable e) {
            Log.e(TAG, e.toString(), e);
            Log.d("API Failure", e.getMessage());
            callback.onFailure(call, e);
        }
    }*/





}