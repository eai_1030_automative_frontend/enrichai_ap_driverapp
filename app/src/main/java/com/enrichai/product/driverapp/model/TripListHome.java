
package com.enrichai.product.driverapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TripListHome implements Serializable {
    @SerializedName("tripId")
    private Integer tripId;
    @SerializedName("pickupLocation")
    private String pickup;
    @SerializedName("deliveryLocation")
    private String delivery;
    @SerializedName("pickupDate")
    private String pickupDate;
    @SerializedName("pickupTime")
    private String pickupTime;
    @SerializedName("distanceCover")
    private String distanceCover;
    @SerializedName("timeTaken")
    private String timeTaken;
    @SerializedName("status")
    private String status;

    public TripListHome(Integer tripId, String pickup, String delivery, String pickupDate, String pickupTime, String distanceCover, String timeTaken, String status) {
        this.tripId = tripId;
        this.pickup = pickup;
        this.delivery = delivery;
        this.pickupDate = pickupDate;
        this.pickupTime = pickupTime;
        this.distanceCover = distanceCover;
        this.timeTaken = timeTaken;
        this.status = status;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer taskId) {
        this.tripId = taskId;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDistanceCover() {
        return distanceCover;
    }

    public void setDistanceCover(String distanceCover) {
        this.distanceCover = distanceCover;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    @Override
    public String toString() {
        return "TripListHome{" +
                "tripId=" + tripId +
                ", pickup='" + pickup + '\'' +
                ", delivery='" + delivery + '\'' +
                ", pickupDate='" + pickupDate + '\'' +
                ", pickupTime='" + pickupTime + '\'' +
                ", distanceCover='" + distanceCover + '\'' +
                ", timeTaken='" + timeTaken + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
