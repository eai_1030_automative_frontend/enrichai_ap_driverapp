package com.enrichai.product.driverapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.enrichai.product.driverapp.R;


public class WelcomeScreen extends DefaultLayout {
    private static final String TAG = WelcomeScreen.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_welcome);
        actionBarSetup(TAG);
    }

    public void Signup(View view) {
        Intent intent =new Intent (this,SignUp.class);
        startActivity (intent);
    }
    public void Login(View view) {
        Intent intent =new Intent (this,LoginActivity.class);
        startActivity (intent);
    }
}
