package com.enrichai.product.driverapp.customLayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
public class BehaviourFragment extends Fragment {

    TextView rating;
    TextView ratingMsg;
    RatingBar ratingBar;
    TextView ratingComment;

    public BehaviourFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rating = (TextView) container.findViewById(R.id.rating);
        ratingMsg = (TextView) container.findViewById(R.id.rating_msg);
        ratingBar = (RatingBar) container.findViewById(R.id.rating_bar);
        ratingComment = (TextView) container.findViewById(R.id.rating_comment);

        rating.setText("7");
        ratingMsg.setText("Your Driving Score");
        ratingBar.setRating(3);
        ratingComment.setText("Well driven! Push the limits!");
        return inflater.inflate(R.layout.fragment_behaviour, container, false);
    }

}