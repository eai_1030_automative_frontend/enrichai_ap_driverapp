package com.enrichai.product.driverapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;


public class SignUp extends DefaultLayout {
    private static final String TAG = SignUp.class.getSimpleName();
    private Button btnCreateAccount;
    private TextView txtSignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_sign_up);
        initLayoutVariable();
    }

    private void initLayoutVariable() {
        btnCreateAccount = (Button) findViewById (R.id.btnCreateAccount);
        txtSignin = (TextView) findViewById (R.id.txtSignin);
        actionBarSetup(TAG);
        if (Methods.isNetworkAvailable()) {
            initOnClickListner();
        } else {
            Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
        }
    }

    private void initOnClickListner() {
        btnCreateAccount.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                CreateAcc();
            }
        });
        txtSignin.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                SignInAllReadyHave();
            }
        });
    }

    public void SignInAllReadyHave() {
        Intent intent =new Intent (this,LoginActivity.class);
        startActivity (intent);
    }

    public void CreateAcc() {
        Intent intent =new Intent (this,MainActivity.class);
        startActivity (intent);
    }
}
