package com.enrichai.product.driverapp.activity;

import android.os.Bundle;

import com.enrichai.product.driverapp.R;


public class ProfileActivity extends DefaultLayout {
    private static final String TAG = ProfileActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_profile);
        actionBarSetup(TAG);
    }
}
