package com.enrichai.product.driverapp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.adapter.RecyclerViewTripData;
import com.enrichai.product.driverapp.model.TripList;

import java.util.ArrayList;

public class TripActivity extends DefaultLayout {
    private static final String TAG = TripActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_my_trips);
        actionBarSetup(TAG);
        initRecycleView();

    }

    private void initRecycleView() {
        ArrayList<TripList> tripListData= new ArrayList<TripList>();
        tripListData.add(new TripList("xyz","mno","accepted"));

        tripListData.add(new TripList("pno","mna","accepted"));

        tripListData.add(new TripList("wrla","ada","rejected"));

        tripListData.add(new TripList("prya","assadsad","accepted"));
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_trip_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerViewTripData(tripListData);
        mRecyclerView.setAdapter(mAdapter);
    }
}
