package com.enrichai.product.driverapp.activity;

import android.os.Bundle;

import com.enrichai.product.driverapp.R;

public class PickupActivity extends DefaultLayout {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup);
        actionBarSetup("Default");
    }
}
