package com.enrichai.product.driverapp.adapter;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.activity.TakeAction;
import com.enrichai.product.driverapp.init.EnrichlabApplication;
import com.enrichai.product.driverapp.model.TripListHome;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;

import java.util.ArrayList;

public class RecycleViewNewTrip extends RecyclerView.Adapter<RecycleViewNewTrip
        .DataObjectHolder> {
    private static String TAG = "RecycleViewNewTrip";
    private ArrayList<TripListHome> mDataset;
    private static RecycleViewNewTrip.MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView new_trip_location,new_trip_distance,new_trip_timetaken,new_trip_time_remaining;
        Button takeAction;
        public DataObjectHolder(View itemView) {
            super(itemView);
            new_trip_location = (TextView) itemView.findViewById(R.id.new_trip_location);
            new_trip_distance = (TextView) itemView.findViewById(R.id.new_trip_distance);
            new_trip_timetaken = (TextView) itemView.findViewById(R.id.new_trip_timetaken);
            new_trip_time_remaining = (TextView) itemView.findViewById(R.id.new_trip_time_remaining);
            takeAction = (Button) itemView.findViewById(R.id.takeAction);
            takeAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(RecycleViewNewTrip.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecycleViewNewTrip(ArrayList<TripListHome> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecycleViewNewTrip.DataObjectHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_home_new_trip, parent, false);

        RecycleViewNewTrip.DataObjectHolder dataObjectHolder = new RecycleViewNewTrip.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(RecycleViewNewTrip.DataObjectHolder holder, int position) {
        holder.new_trip_location.setText(mDataset.get(position).getPickup()+" to "+mDataset.get(position).getDelivery());
        holder.new_trip_distance.setText(mDataset.get(position).getDistanceCover());
        holder.new_trip_timetaken.setText(mDataset.get(position).getTimeTaken());
        //String status = mDataset.get(position).getTripStatus();
        holder.takeAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Methods.isNetworkAvailable()) {
                    Intent intent = new Intent(EnrichlabApplication.getAppContext(), TakeAction.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("action", "timer");
                    EnrichlabApplication.getAppContext().startActivity(intent);
                } else {
                    Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
                }
            }
        });
    }



    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}