package com.enrichai.product.driverapp.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.utils.Constant;

/**
 * Created by Priyank on 14-06-2018.
 */

public class DefaultLayout extends AppCompatActivity {
      public void actionBarSetup(String activityName) {
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.layout_app_logo);
            getSupportActionBar().setTitle(null);
            switch (activityName) {
                case Constant.WELCOME_SCREEN : break;
                case Constant.SIGN_UP: break;
                case Constant.LOGIN_ACTIVITY: break;
                case Constant.MAIN_ACTIVITY: break;
                case Constant.PROFILE_ACTIVITY : break;
                case Constant.TRIP_ACTIVITY :break;
                case Constant.TAKE_ACTION : break;
                case "Deafult" : break;
                /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);*/
                
            }
        }
    }
}
