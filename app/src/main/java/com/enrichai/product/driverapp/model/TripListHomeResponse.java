package com.enrichai.product.driverapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TripListHomeResponse implements Serializable {
    @SerializedName("tripList")
    private List<TripListHome> tripLis;

    public List<TripListHome> getTripLis() {
        return tripLis;
    }

    public void setTripLis(List<TripListHome> tripLis) {
        this.tripLis = tripLis;
    }
}
