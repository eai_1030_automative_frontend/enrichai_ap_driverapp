package com.enrichai.product.driverapp.model;

public class TripList {
    private String pickAddr;
    private String destAddr;
    private String tripStatus;

    public TripList(String pickAddr, String destAddr, String tripStatus) {
        this.pickAddr = pickAddr;
        this.destAddr = destAddr;
        this.tripStatus = tripStatus;
    }

    public String getPickAddr() {
        return pickAddr;
    }

    public void setPickAddr(String pickAddr) {
        this.pickAddr = pickAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    @Override
    public String toString() {
        return "TripList{" +
                "pickAddr='" + pickAddr + '\'' +
                ", destAddr='" + destAddr + '\'' +
                ", tripStatus='" + tripStatus + '\'' +
                '}';
    }
}
