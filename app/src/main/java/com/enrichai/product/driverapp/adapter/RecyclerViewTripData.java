package com.enrichai.product.driverapp.adapter;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.activity.TripDetails;
import com.enrichai.product.driverapp.init.EnrichlabApplication;
import com.enrichai.product.driverapp.model.TripList;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;

import java.util.ArrayList;

public class RecyclerViewTripData extends RecyclerView
        .Adapter<RecyclerViewTripData
        .DataObjectHolder> {
    private static String LOG_TAG = "RecyclerViewHistoryData";
    private ArrayList<TripList> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView destAddr;
        TextView pickAddr;
        View orderStatus;
        public DataObjectHolder(View itemView) {
            super(itemView);
            pickAddr = (TextView) itemView.findViewById(R.id.pickAddr);
            destAddr = (TextView) itemView.findViewById(R.id.destAddr);
            orderStatus = (View) itemView.findViewById(R.id.v_trip_status);
           itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerViewTripData(ArrayList<TripList> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_trip_card_view, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.pickAddr.setText(mDataset.get(position).getPickAddr());
     //   holder.sourceAddr.setText(mDataset.get(position).getSource());
        holder.destAddr.setText(mDataset.get(position).getDestAddr());
        //final String key_id = mDataset.get(position).getOrderId();
        String status = mDataset.get(position).getTripStatus();
        if(status.equals("accepted")){
            holder.orderStatus.setBackgroundColor(EnrichlabApplication.getAppContext().getColor(R.color.button_background_green));
        }else{
            holder.orderStatus.setBackgroundColor(EnrichlabApplication.getAppContext().getColor(R.color.button_background_red));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // open another activity on item click
                if (Methods.isNetworkAvailable()) {
                    Intent intent = new Intent(EnrichlabApplication.getAppContext(), TripDetails.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    EnrichlabApplication.getAppContext().startActivity(intent);
                } else {
                    Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
                }
            }
        });
    }



    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}