package com.enrichai.product.driverapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Space;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.customLayout.NewTripFragment;
import com.enrichai.product.driverapp.customLayout.AssignedTripFragment;
import com.enrichai.product.driverapp.init.EnrichlabApplication;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends DefaultLayout {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Space spaceLeftTop;
    private View navLeftLay;
    private NavigationView navigationViewLeft;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private TextView newTrip, assignedTrip;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLayoutVariable();
    }

    private void initLayoutVariable() {
        intent = new Intent(this,TakeAction.class);
        setupActionBar();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        setupTabView();
        setupDrawer();

    }

    private void setupDrawer() {
        navigationViewLeft = (NavigationView) findViewById(R.id.nav_view2);
        navLeftLay = navigationViewLeft.getHeaderView(0);
        spaceLeftTop = (Space) navLeftLay.findViewById(R.id.spaceLeftTop);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            spaceLeftTop.setVisibility(View.VISIBLE);
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout2);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (Methods.isNetworkAvailable()) {
            setClickOnDrawerItem();
        } else {
            Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
        }
    }

    private void setClickOnDrawerItem() {
        navigationViewLeft.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.profile:
                        Intent profile = new Intent(EnrichlabApplication.getAppContext(), ProfileActivity.class);
                        startActivity(profile);
                        break;
                    case R.id.trip:
                        Intent trip = new Intent(EnrichlabApplication.getAppContext(), TripActivity.class);
                        startActivity(trip);
                        break;
                }
                return false;
            }
        });
    }

    private void setupTabView() {
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //setupTabIcons();
    }

    private void setupActionBar() {
        //actionBarSetup(TAG);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void sendToProfile(View v) {
        Intent intent = new Intent(EnrichlabApplication.getAppContext(), ProfileActivity.class);
        startActivity(intent);
    }

    public void sendToTrip(View v) {
        Intent intent = new Intent(EnrichlabApplication.getAppContext(), TripDetails.class);
        startActivity(intent);
    }

    private void setupTabIcons() {
        newTrip = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        newTrip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.truck, 0, 0);
        tabLayout.getTabAt(0).setCustomView(newTrip);

        assignedTrip = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        assignedTrip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sma, 0, 0);
        tabLayout.getTabAt(1).setCustomView(assignedTrip);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new NewTripFragment(), "New Trips(4)");
        adapter.addFrag(new AssignedTripFragment(), "Accepted Trip(4)");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}