package com.enrichai.product.driverapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;


public class TakeAction extends DefaultLayout {
    private static final String TAG = TakeAction.class.getSimpleName();
    Button acceptButton;
    Button rejectButton;
    TextView tripAcceptHeader;
    RelativeLayout relativeLayout;
    int i=-1;
    //ProgressBar mProgressBar;
    //private TextView textViewShowTime;
    private CountDownTimer countDownTimer; // built in android class
    // CountDownTimer
    private long totalTimeCountInMilliseconds; // total count down time in
    // milliseconds
    private long timeBlinkInMilliseconds; // start time of start blinking
    private boolean blink; // controls the blinking .. on and off


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_action);
        actionBarSetup(TAG);
        initLayoutVariable();
    }

    private void initLayoutVariable() {
        acceptButton = (Button) findViewById(R.id.accept_button);
        rejectButton = (Button) findViewById(R.id.reject_button);
        tripAcceptHeader = (TextView) findViewById(R.id.trip_accept_header);
        //textViewShowTime = (TextView) findViewById(R.id.tvTimeCount);
        //mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        relativeLayout = (RelativeLayout) findViewById(R.id.instantTimer);
        String action = getIntent().getStringExtra("action");

        if(action.equals("timer")){
            relativeLayout.setVisibility(View.VISIBLE);
            acceptButton.setText("Accept");
            acceptButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    //launchConfirmationDialog("accept");
                }
            });

            rejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                    //launchConfirmationDialog("reject");
                }
            });
            //setTimer();
        }else if(action.equals("start")){
            acceptButton.setText("Accept");
            relativeLayout.setVisibility(View.GONE);
            acceptButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        }else{
            acceptButton.setVisibility(View.GONE);
            tripAcceptHeader.setText("Accepted Trip Detail");
            relativeLayout.setVisibility(View.GONE);
        }
    }


   /* private void setTimer() {
        int time = 15;
        mProgressBar.setMax(60*time);
        totalTimeCountInMilliseconds = 60 * time * 1000;

        timeBlinkInMilliseconds = 30 * 1000;
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 500) {
            // 500 means, onTick function will be called at every 500
            // milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                //i++;
                //Setting the Progress Bar to decrease wih the timer
                mProgressBar.setProgress((int) (leftTimeInMilliseconds / 1000));
                textViewShowTime.setTextAppearance(getApplicationContext(),R.style.normalText);


                if (leftTimeInMilliseconds < timeBlinkInMilliseconds) {
                    textViewShowTime.setTextAppearance(getApplicationContext(),R.style.blinkText);
                    // change the style of the textview .. giving a red
                    // alert style

                    if (blink) {
                        textViewShowTime.setVisibility(View.VISIBLE);
                        // if blink is true, textview will be visible
                    } else {
                        textViewShowTime.setVisibility(View.INVISIBLE);
                    }

                    blink = !blink; // toggle the value of blink
                }

                textViewShowTime.setText(String.format("%02d", seconds / 60)
                        + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format

            }

            @Override
            public void onFinish() {
                // this function will be called when the timecount is finished
                textViewShowTime.setText("Time up!");
                textViewShowTime.setVisibility(View.VISIBLE);
            }

        }.start();

    }*/
}
