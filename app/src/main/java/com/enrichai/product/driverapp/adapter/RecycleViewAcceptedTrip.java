package com.enrichai.product.driverapp.adapter;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.activity.PickupActivity;
import com.enrichai.product.driverapp.activity.TakeAction;
import com.enrichai.product.driverapp.init.EnrichlabApplication;
import com.enrichai.product.driverapp.model.TripListHome;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;

import java.util.ArrayList;

public class RecycleViewAcceptedTrip extends RecyclerView.Adapter<RecycleViewAcceptedTrip.DataObjectHolder> {
    private static String TAG = "RecycleViewAcceptedTrip";
    private ArrayList<TripListHome> mDataset;
    private static RecycleViewAcceptedTrip.MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        LinearLayout l_timeLeft;
        TextView number_of_days,accept_trip_location,accept_trip_date_time,timeRemaining,days_text;
        RelativeLayout r_timeRemaining,r_alert_text;
        ImageView img_chevron;
        public DataObjectHolder(View itemView) {
            super(itemView);
            l_timeLeft = (LinearLayout) itemView.findViewById(R.id.l_timeLeft);
            r_timeRemaining = (RelativeLayout) itemView.findViewById(R.id.r_timeRemaining);
            r_alert_text = (RelativeLayout) itemView.findViewById(R.id.r_alert_text);
            number_of_days = (TextView) itemView.findViewById(R.id.number_of_days);
            accept_trip_location = (TextView) itemView.findViewById(R.id.accept_trip_location);
            accept_trip_date_time = (TextView) itemView.findViewById(R.id.accept_trip_date_time);
            timeRemaining = (TextView) itemView.findViewById(R.id.timeRemaining);
            days_text = (TextView) itemView.findViewById(R.id.days_text);
            img_chevron = (ImageView) itemView.findViewById(R.id.img_chevron);

        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(RecycleViewAcceptedTrip.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecycleViewAcceptedTrip(ArrayList<TripListHome> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecycleViewAcceptedTrip.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_home_accept_trip, parent, false);

        RecycleViewAcceptedTrip.DataObjectHolder dataObjectHolder = new RecycleViewAcceptedTrip.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(RecycleViewAcceptedTrip.DataObjectHolder holder, int position) {
        String pickupDate = mDataset.get(position).getPickupDate();
        String status = "pending";
        if(Methods.dateCurrentDiffernce(pickupDate).equals("0")){
            holder.r_timeRemaining.setVisibility(View.VISIBLE);
            holder.l_timeLeft.setBackgroundColor(EnrichlabApplication.getAppContext().getColor(R.color.activity3colorPrimary));
            holder.number_of_days.setTextColor(EnrichlabApplication.getAppContext().getColor(R.color.activity3colorPrimaryDark));
            holder.days_text.setTextColor(EnrichlabApplication.getAppContext().getColor(R.color.activity3colorPrimaryDark));
            holder.img_chevron.setImageResource(R.drawable.ic_chevron_right_red_24dp);
            status = "start";
        }else{
            holder.r_alert_text.setVisibility(View.VISIBLE);
        }
        holder.number_of_days.setText(Methods.dateCurrentDiffernce(pickupDate));
        holder.accept_trip_location.setText(mDataset.get(position).getPickup() + " to "+mDataset.get(position).getDelivery());
        holder.accept_trip_date_time.setText(mDataset.get(position).getPickupDate() + " "+mDataset.get(position).getPickupTime());

        String finalStatus = status;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Methods.isNetworkAvailable()) {
                    Intent intent;
                    if(finalStatus.equals("start")){
                        intent = new Intent(EnrichlabApplication.getAppContext(), PickupActivity.class);
                    }else{
                        intent = new Intent(EnrichlabApplication.getAppContext(), TakeAction.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("action", finalStatus);
                    EnrichlabApplication.getAppContext().startActivity(intent);
                } else {
                    Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
                }
            }
        });

        //holder.timeRemaining.setText();
        //String status = mDataset.get(position).getTripStatus();
        /*holder.takeAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // open another activity on item click
               *//* if (Methods.isNetworkAvailable()) {
                    Intent intent = new Intent(EnrichlabApplication.getAppContext(), TripDetails.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    EnrichlabApplication.getAppContext().startActivity(intent);
                } else {
                    Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
                }*//*
            }
        });*/
    }



    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}