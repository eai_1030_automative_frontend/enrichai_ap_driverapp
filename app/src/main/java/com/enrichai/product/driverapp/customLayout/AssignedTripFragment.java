package com.enrichai.product.driverapp.customLayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.activity.TripActivity;
import com.enrichai.product.driverapp.adapter.RecycleViewAcceptedTrip;
import com.enrichai.product.driverapp.model.TripListHome;

import java.util.ArrayList;

public class AssignedTripFragment extends Fragment {
    private static final String TAG = TripActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TabLayout tabLayout;
    private TabLayout.Tab tab;

    public AssignedTripFragment() {
        // Required empty public constructor
    }
// changes to check branch merge thrpough pull requests
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_assign_trip, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_home_assign);
        /*tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        TabLayout.Tab tab = tabLayout.getTabAt(0);*/
        initRecycleView();

    }
    private void initRecycleView() {
        ArrayList<TripListHome> tripListData= new ArrayList<TripListHome>();
        tripListData.add(new TripListHome(121,"Gurgaon","Noida","04/07/2018","09:00 AM","60 Kms","1.30 Hrs", "new"));
        tripListData.add(new TripListHome(123,"Noida","Gurgaon","05/07/2018","09:00 AM","60 Kms","1.30 Hrs", "new"));
        tripListData.add(new TripListHome(124,"Gurgaon","Neem Ka Thana","15/07/2018","09:00 AM","60 Kms","1.30 Hrs", "new"));
        tripListData.add(new TripListHome(125,"Neem Ka Thana","Noida","16/07/2018","09:00 AM","60 Kms","1.30 Hrs", "new"));

        //tab.setText("New Trip("+tripListData.size()+")");
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecycleViewAcceptedTrip(tripListData);
        mRecyclerView.setAdapter(mAdapter);
    }
}