package com.enrichai.product.driverapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.enrichai.product.driverapp.R;
import com.enrichai.product.driverapp.utils.Constant;
import com.enrichai.product.driverapp.utils.Methods;


public class LoginActivity extends DefaultLayout {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private TextView forgetPassword;
    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_login);
        initLayoutVariable();
    }

    private void initLayoutVariable() {
        forgetPassword = (TextView) findViewById (R.id.forgetPassword);
        btnLogin = (Button) findViewById (R.id.btnLogin);
        actionBarSetup(TAG);
        if (Methods.isNetworkAvailable()) {
            initOnClickListner();
        } else {
            Methods.showPromptMessage(Constant.INTERNET_CONNECTION);
        }
    }

    private void initOnClickListner() {
        forgetPassword.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                ForgetPassword();
            }
        });
        btnLogin.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
    }

    public void ForgetPassword(){
        Methods.showPromptMessage(Constant.FORGET_PASSWORD);
    }
    public void Login(){
        Intent intent =new Intent (this,MainActivity.class);
        startActivity (intent);
    }
}
